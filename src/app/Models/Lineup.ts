export class Player{
"club_slug"!: string;
"logo_club"!: string;
"club_id"!: number;
"nom"!: string;
"prenom"!: string;
"img":string;
"post": string;
"equipe_id": number;


}
export interface Positions{
poste:string;
players:Player[];
}
export interface Planselect {
  key: string;
  value: string;
}
export class Block {
  block!: number;
  positions!: Positions[];
}
export class Plan {
  blocks: Block[] = [];
  blocks_h!:number;
}
