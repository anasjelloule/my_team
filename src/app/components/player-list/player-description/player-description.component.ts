import { Component, EventEmitter, Input,Output } from '@angular/core';
import { Player } from 'src/app/Models/Lineup';
@Component({
  selector: 'app-player-description',
  templateUrl: './player-description.component.html',
  styleUrls: ['./player-description.component.scss'],
})
export class PlayerDescriptionComponent {
  @Input() player!: Player;
  @Output() removeplayerevent = new EventEmitter<Player>();
  remove(player: Player) {
    this.removeplayerevent.emit(player);
  }
}
