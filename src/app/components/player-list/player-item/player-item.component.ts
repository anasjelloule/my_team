import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import {
  MatDialog,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { Block, Player, Positions } from 'src/app/Models/Lineup';
import { FormPlayerComponent } from './form-player/form-player.component';
import { MatButtonModule } from '@angular/material/button';
import { DeleteModalComponent } from 'src/app/shared/widget/delete-modal/delete-modal.component';
import { NotificationComponent } from 'src/app/shared/widget/notification/notification.component';
import { PlanService } from 'src/app/services/plan.service';

@Component({
  selector: 'app-player-item',
  templateUrl: './player-item.component.html',
  styleUrls: ['./player-item.component.scss'],
  // changeDetection:ChangeDetectionStrategy.OnPush
})
export class PlayerItemComponent implements OnInit {
  players: Player[] = [];
  @Input() block!: Block;
  @Input() position!: Positions;

  constructor(
    private modalService: MatDialog,
    private planservice: PlanService
  ) {}
  ngOnInit(): void {
    // console.log("call")
  }

  add() {
    if (this.players.length >= 2) {
      const dialogRef = this.modalService.open(NotificationComponent, {
        width: '350px',
      });
      dialogRef.componentInstance.title = 'Notification';
      dialogRef.componentInstance.body = 'Reached Out Limit Of 2';
    } else
      this.modalService
        .open(FormPlayerComponent, {
          width: '100%',
          position: { top: '5%' },
        })
        .afterClosed()
        .subscribe({
          next: (data) => {
            if (data) {
              this.players.push(data);
              // console.log(this.players);
              this.position.players = this.players;
              // this.block.positions.push(this.position);
              // let block=
              // this.planservice.addblocktoplan(this.position);

              // console.log(this.block);
            }
          },
        });
  }

  removeplayer($event: Player) {
    this.modalService
      .open(DeleteModalComponent, {
        width: '350px',
      })
      .afterClosed()
      .subscribe({
        next: (data) => {
          if (!data) return;
          const index = this.players.findIndex(
            (p) =>
              p.nom === $event.nom &&
              p.prenom == $event.prenom &&
              p.club_id == $event.club_id
          );

          if (index !== -1) {
            this.players.splice(index, 1);
            // this.position.players=this.players;
          }
        },
      });
  }
  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.players, event.previousIndex, event.currentIndex);
  }
}
