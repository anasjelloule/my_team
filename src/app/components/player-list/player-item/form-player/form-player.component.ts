import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import {
  debounceTime,
  tap,
  switchMap,
  finalize,
  distinctUntilChanged,
  filter,
} from 'rxjs/operators';
import { Player } from 'src/app/Models/Lineup';
import { PlayersService } from 'src/app/services/players.service';



@Component({
  selector: 'app-form-player',
  templateUrl: './form-player.component.html',
  styleUrls: ['./form-player.component.scss'],
})
export class FormPlayerComponent implements OnInit {
  searchPlayers = new FormControl();
  filteredPlayers: Player[] = [];
  isLoading = false;
  errorMsg!: string;
  minLengthTerm = 3;
  selectedPlayer!: Player;

  constructor(
    private playerservice: PlayersService,
    private _dialogRef: MatDialogRef<FormPlayerComponent>
  ) {}
  ngOnInit(): void {
    this.searchPlayers.valueChanges
      .pipe(
        filter((res) => {
          return res !== null && res.length >= this.minLengthTerm;
        }),
        distinctUntilChanged(),
        debounceTime(1000),
        tap(() => {
          this.errorMsg = '';
          this.filteredPlayers = [];
          this.isLoading = true;
        }),
        switchMap((full: string) =>
          this.playerservice.getPlayerByName(full).pipe(
            finalize(() => {
              this.isLoading = false;
            })
          )
        )
      )
      .subscribe({
        next: (data: Player[]) => {
          if (data.length === 0) {
            this.errorMsg = 'Not Found!';
            this.filteredPlayers = [];
          } else {
            this.errorMsg = '';
            this.filteredPlayers = data;
          }
        },
        error: (err) => (this.errorMsg = err),
      });
  }

  onSelected($event:any) {
    this.selectedPlayer = $event.option.value;
    this._dialogRef.close(this.selectedPlayer);
  }
  selected(player:Player){
 this.selectedPlayer=player;
  }

  displayWith(value: Player) {
    return value?.nom;
  }

  clearSelection() {
    this.selectedPlayer = new Player();
    this.filteredPlayers = [];
  }
}
