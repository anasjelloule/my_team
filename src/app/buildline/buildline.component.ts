import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { PlanService } from '../services/plan.service';
import { Plan, Planselect } from '../Models/Lineup';
import { screen } from '../state/global.state';
import { PlayersService } from '../services/players.service';
import { Subject, takeUntil } from 'rxjs';
import data_play from './../../../DB/system-play.json';
@Component({
  selector: 'app-buildline',
  templateUrl: './buildline.component.html',
  styleUrls: ['./buildline.component.scss'],
})
export class BuildlineComponent implements OnInit, OnDestroy {
  Screen: screen = new screen();
  plan!: Plan;
  blocks_h: number = 0;

  Bgimgs = [
    { key: 'Vert', url: './assets/img/bg.svg' },
    {
      key: 'Light Green',
      url: 'https://www.buildlineup.com/soccer-field-light.png',
    },
  ];
  zoom=100;
  Zoom = [
    { key: '0.5', value: '50%' },
    { key: '0.75', value: '75%' },
    { key: '1', value: '100%' },
    { key: '1.25', value: '125%' },
    { key: '1.5', value: '150%' },
  ];

  Pcounts: any[] = data_play.systems;
  plans: any[] = data_play.systems[0].system_play;
  selectedPlan = '5-4-1';
  selectedZoom = '1';
  selectedBgimgs = './assets/img/bg.svg';
  selectedPcount = 11;

  private _unsubscribeAll: Subject<any> = new Subject();

  constructor(
    private playerservice: PlayersService,
    private planservice: PlanService
  ) {}

  // ngAfterViewInit() {
  //   this.compistion.optionSelectionChanges.subscribe({
  //     next: (data: any) => {
  //       if (data.isUserInput) console.log(data);
  //     },
  //   });
  // }

  ngOnInit(): void {
    // this.changeplan('4231');
    this.changeplan(this.selectedPlan);
  }
  formatLabel(value: number): string {
    if (value >= 1000) {
      return Math.round(value / 1000) + 'k';
    }

    return `${value}`;
  }
  onSelectionPcountChange($event: any) {
    // this.changeplan(this.selectedPlan);
    // console.log($event)
    // console.log(this.plans.find(p=>p.));
    //  this.Pcounts=data_play.systems.find(s=>s.nbr===$event)?.system_play.map();

    this.plans = data_play.systems.find((s) => s.nbr === $event)?.system_play!;

    this.selectedPlan = this.plans[0].label;
    // console.log($event);
  }

  onSelectionPlanChange($event: any) {
    this.changeplan(this.selectedPlan);
  }

  onSelectionZoomChange($event: any) {
    // console.log($event)
    this.selectedZoom = $event;
  }

  onSelectionBgimgChange($event: any) {
    // console.log($event)
    this.selectedBgimgs = $event;
  }

  changeplan(plan: string): void {
    this.plan = this.planservice.gnerateplan(plan);
    this.blocks_h = Math.floor(this.Screen.height / this.plan.blocks_h);
    // .pipe(takeUntil(this._unsubscribeAll))
    // .subscribe({
    //   next: (data) => {
    //     console.log(data);
    //     this.plan = data;
    //     this.blocks_h = Math.floor(this.Screen.height / this.plan.blocks_h);
    //   },
    //   error: (err) => {
    //     console.error(err);
    //   },
    // });
  }

  save() {
    this.planservice.getplan().subscribe({
      next: (result) => {
        console.log(result);
        alert('Send plan successfully');
        // result.blocks.map(b.positions.);
      },
    });
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next(null);
    this._unsubscribeAll.complete();
  }
}
