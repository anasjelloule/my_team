import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeleteModalComponent } from './delete-modal/delete-modal.component';
import { NotificationComponent } from './notification/notification.component';



@NgModule({
  declarations: [
    DeleteModalComponent,
    NotificationComponent
  ],
  imports: [
    CommonModule
  ]
})
export class WidgetModule { }
