import { Component, Input } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { CommonModule } from '@angular/common';
@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
  imports: [
    MatDialogModule,
    MatButtonModule,
    CommonModule,
  ],
  standalone: true,
})
export class NotificationComponent {
  @Input() title!: string;
  @Input() body!: string;
  @Input() time: number = 1500;
  @Input() center: 'start' | 'center' | 'end' | undefined = 'center';

  constructor(public dialogRef: MatDialogRef<NotificationComponent>) {
    dialogRef.afterOpened().subscribe((_) => {
      setTimeout(() => {
        // dialogRef.close();
      }, this.time);
    });
  }
  confirmclose() {
    this.dialogRef.close(true);
  }
}
