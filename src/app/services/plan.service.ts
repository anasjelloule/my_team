import { Injectable, OnDestroy } from '@angular/core';
import { Observable, Subject, BehaviorSubject, of, takeUntil } from 'rxjs';
import { PlayersService } from './players.service';

import { Player, Plan, Planselect, Block, Positions } from '../Models/Lineup';

@Injectable({
  providedIn: 'root',
})
export class PlanService {
  private plan: Plan = new Plan();
  private plan$ = new BehaviorSubject<Plan>(new Plan());
  private players!: Player[];

  constructor(private playerservice: PlayersService) {}

  // getplan(plan: string): Observable<Plan> {
  //   this.playerservice
  //     .getplayers()
  //     .pipe(takeUntil(this._unsubscribeAll))
  //     .subscribe({
  //       next: (data) => {
  //         this.players = data;
  //         this.switchplan(plan);
  //       },
  //       error: (err) => {
  //         console.error(err);
  //       },
  //     });

  //   return of(this.plan);
  // }

  gnerateplan(formation: string): Plan {
    let nwplan = new Plan();
    // GET BLOCKS LENGTH WITHOUT GK BLOCK
    let blocks = formation.split('-').reverse();
    // CALCULATE BLOCKS HIGH BY NOMBRE OF BLOCK
    nwplan.blocks_h = blocks?.length+1;
    let lst;
    // GENERATE BLOCKS
    blocks.map((b, index) => {
      nwplan.blocks.push({ block: index + 1, positions: Array(+b) });
      lst = index + 1;
    });

    // ADD GK BLOCK
    nwplan.blocks.push({ block: lst! + 1, positions: Array(1) });

    // GENERATE PLAYERS IN THE BLOCK BY POSTE
    nwplan.blocks.map((b) => {
      for (let i = 0; i < b.positions.length; i++) {
        b.positions[i] = { poste: `poste-${i + 1}`, players: [] };
      }
    });

    this.plan$.next(nwplan);
    return nwplan;
  }
  getplan(): Observable<Plan> {
    return this.plan$;
  }
  addblocktoplan(postion: Positions) {
    // this.plan.blocks.map((p) => console.log(p));
    console.log(postion);
    // console.log(block);
  }

  // ngOnDestroy(): void {
  //   this._unsubscribeAll.next(null);
  //   this._unsubscribeAll.complete();
  // }

  // switchplan(plan: string): void {
  //   switch (plan) {
  //     case '4231':
  //       this.plan = {
  //         blocks: [
  //           {
  //             block: 0,
  //             positions: [
  //               {
  //                 poste: 'A',
  //                 players: [],
  //               },
  //             ],
  //           },
  //           {
  //             block: 1,
  //             positions: [
  //               {
  //                 poste: 'A',
  //                 players: [],
  //               },
  //               {
  //                 poste: 'B',
  //                 players: [],
  //               },
  //               {
  //                 poste: 'C',
  //                 players: [],
  //               },
  //             ],
  //           },
  //           {
  //             block: 2,
  //             positions: [
  //               {
  //                 poste: 'A',
  //                 players: [],
  //               },
  //               {
  //                 poste: 'B',
  //                 players: [],
  //               },
  //             ],
  //           },
  //           {
  //             block: 3,
  //             positions: [
  //               {
  //                 poste: 'A',
  //                 players: [],
  //               },
  //               {
  //                 poste: 'B',
  //                 players: [],
  //               },
  //               {
  //                 poste: 'C',
  //                 players: [],
  //               },
  //               {
  //                 poste: 'D',
  //                 players: [],
  //               },
  //             ],
  //           },
  //           {
  //             block: 4,
  //             positions: [
  //               {
  //                 poste: 'A',
  //                 players: [],
  //               },
  //             ],
  //           },
  //         ],
  //         blocks_h: 5,
  //       };

  //       break;
  //   }
  // }
}
